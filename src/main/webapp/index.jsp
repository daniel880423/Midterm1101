<%-- 
    Document   : index
    Created on : Nov 8, 2021, 1:10:37 PM
    Author     : lendle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1 style="color:red">Food !</h1>
        <%
            String pic = "question";
            /*
            Q1.
            從 cookie 中找出 “food” 的內容，
            並將其儲存至 pic 變數中 （30%）
            */
            Cookie [] cookies=request.getCookies();
            for(Cookie c:cookies){
                if(c.getName().equals("pic")){
                    pic=c.getValue();
                    break;
                }
            } 

            
        %>
        <img style="position: absolute; left: 200px; right: 200px; top: 200px; bottom: 300px" src="toppng.com-hands-960x569.png"></img>
        <!--
        Q2.
        底下 img 的 src，請用 jsp 顯示 pic 變數的內容 （20%）
        -->
        <a href="question.jsp"></a>
        
        <img id="foodImage" style="position: absolute; left: 500px; top: 450px; width: 300px" src="question.png"></img>
        <p>
        <!--
        Q3.
        將表單指定成 GET 的另外一種 (10%)
        
        Q4.
        將表單送出後指定到 SetFoodServlet (10%)
        -->
        
        <form action="setFood" method="POST">
            Your Favorite Food: <input type="text" name="pic" value=""/>
            <input type="submit"/>
        </form>
        
        <form>
            Choose: <select name="food">
                <option value="png-clipart-hamburger-hamburger-food">漢堡</option>
                <option value="ramen">拉麪</option>
                <option value="chicken">炸雞</option>
            </select>
            <input type="submit"/>
        </form>
    </p>
</body>
</html>
